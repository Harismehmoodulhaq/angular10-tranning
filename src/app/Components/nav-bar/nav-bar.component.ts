import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {


  appName = 'Little Coder Angular App';
  myName = 'Little Coder';

  constructor() { }

  ngOnInit(): void {
    
  }

  getName() {
    return this.myName;
  }

}
